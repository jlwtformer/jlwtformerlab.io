# Blog

[RSS Feed](/feed.xml) is available.

## October 2024

[Creating A COSMIC App: First Steps](blogs/cosmic-app-first-steps.html)

## August 2024

[How I Run My Website (2024 Edition)](blogs/how-website-2024.html)

[Review of Ente Photos, A Private Google And iCloud Photos Alternative](blogs/ente-photos-review.html)

## July 2024

[How Cosmic DE Is More Exciting Than The Latest macOS Updates](blogs/cosmic-better-macos.html)

[Welcome Back Blog](blogs/welcome-back-blog.html)

## June 2023

[Moving From Linux to Mac](blogs/linux-to-macos.html)

## May 2023

[A Guide To Digital Minimalism](blogs/digital-minimalism-guide.html)

## April 2023

[Dell Inspiron 15 Review: A Decade of Utility](blogs/dell-inspiron-15-review.html)

## December 2022

[Building GUI Rust Apps With The Iced Toolkit](blogs/rust-iced-tutorial-1.html)

## April 2022

[Fedora 36 Beta - Seriously Tempting](blogs/fedora-36-beta.html)

## March 2022

[The Things I Like About Manjaro, And The Thing I Don't](blogs/manjaro-review-early-2022.html)

[Learn To Program Your Own Tools: Part 2](blogs/rust-lang-tutorial-p2.html)

## February 2022

[Learn To Program Your Own Tools: Part 1](blogs/rust-lang-tutorial-p1.html)

## December 2021

[Say Hello To helloSystem](/blogs/hello-system-review.html)

## November 2021

[The First Step Into 3D Printing, Know Your Printer](/blogs/3d-printer-first-steps.html)

## October 2021

[Starting Video Logs](/blogs/starting-video-log.html)

[Making A Program To Pick Blog Topics For Me](/blogs/making-topic-picker.html)

## September 2021

[3D Printing Prototypes: Spider-Man Armor](/blogs/prototype-spider-man-add-on.html)

## July 2021

[Creating an RSS Feed for a Static Website](blogs/making-rss-feed.html)

## June 2021

[How and Why I Run This Website](blogs/how-why-website.html)

[Fan Fiction: Star Wars Echoes of the Beyond - Chapter 2](blogs/sweob-c2.html)

[Fan Fiction: Star Wars Echoes of the Beyond - Chapter 1](blogs/sweob-ch1.html)

[Encrypting Email Contents with GnuPGP](blogs/email-pgp.html)

[Sloth and Smell the Roses](blogs/sloth-smell-roses.html)
