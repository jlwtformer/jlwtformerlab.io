# The First Step Into 3D Printing, Know Your Printer

_Thursday, November 4, 2021_

There's no doubt that 3D printers have become a common possession of hobbyists and cosplay enthusiasts all over the globe. The ability to make solid objects out of a simple spool of plastic is such a futuristic advancement to small and large scale manufacturing. With 3D printing starting to enter the world of common home shop equipment, I felt it would be a good idea to share some of the steps and tricks I've discovered in my last half year of printing various props and tools.

There are a plethora of 3D printer models available for purchase. And while some look similar, others look very different. There are a lot of different types available, in fact there are 9 different types:

* Stereolithography (SLA)
* Digital Light Processing (DLP)
* Fused deposition Modeling (FDM)
* Selective Laser Sintering (SLS)
* Selective Laser Melting (SLM)
* Electronic Beam Melting (EBM)
* Laminated Object Manufacturing (LOM)
* Binder Jetting (BJ)
* Material Jetting (MJ)

While that's a lot of options, the most common in the budget printing options is the FDM printer. That's the type of printer that I have, and the one that most at home print enthusiasts will start out with.

The printer I have is a Creality Ender 3. These printers are great for first timers, mainly because of price, but also because they are straight forward to build and maintain. They're also tiny and can be carried in one hand!

Now when I say the printer needs to be built properly, I mean it needs to be put together both how the instruction manual says, but also needs to be tuned to provide the best print performance you can get out of them.

This means leveling the bed, ensuring the motion belts are nice and tight, the hot end won't clog, and the the extruder won't quit out half way through a print. That's a lot of components, but we'll go through them real quick.

### The Basic Components Of A 3D Printer

Now just like the different kind of 3D printers, FDM printers come in all shapes and sizes as well. However, all FDM printers have four main components: a print bed, an extruder, a hot end, and a bowden tube.

The print bed is the large flat surface at the bottom of the printer. It's where the actual print will sit while being printed. Depending on the printer model you purchase, you may have a heated print bed (like on my Ender 3).

_Pictured below: The print bed of an Ender 3._

![Ender 3 Print Bed](/res/3d_printer_bed.jpg)

The extruder is the component that will feed the printing filament through to the hot end. Typically it's located on the side of the printer, and consists of a stepper motor and a clamp to hold the filament to the motor.

_Pictured below: The extruder of an Ender 3._

![Ender 3 Extruder](/res/3d_printer_extruder.jpg)

The hot end is the part of the printer that lays the hot filament down onto the print bed. It's the part that moves the most on the printer.

_Pictured below: The hot end of an Ender 3._

![Ender 3 Hot End](/res/3d_printer_hot_end.jpg)

The part that feeds the filament between the extruder and the hot end is called the bowden tube. It seems like a minor component but this one part can also give you a heap of trouble if not put in correctly.

_Pictured below: The stock white bowden tube of an Ender 3._

![Ender 3 Bowden Tube](/res/3d_printer_bowden_tube.jpg)

With those in mind you should now be better prepared to begin your journey into 3D printing. If you liked this blog post, please let me know! I always welcome feedback.

Thanks,
Jacob
