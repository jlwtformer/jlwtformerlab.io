# Creating A COSMIC App: First Steps

_Friday, October 4, 2024_

Without a doubt the most exciting thing happening in the Linux desktop space this year is the continued growth of the COSMIC Desktop Environment by System 76. And with that growth also comes the growth of third party tools and apps as well, including my own app, [Forecast](https://github.com/cosmic-utils/forecast). And there are more and more developers getting their feet wet with the COSMIC ecosystem and libcosmic every day.

But what if you are interested in developing in libcosmic but aren't sure where to start? Well, I have decided that I am going to blog my creation process for my next COSMIC app, so that anyone can follow along and hopefully create an app of their own! So let's get started at square one.

## Square One: Getting An Idea

Before you can write your first line of code you need to have an idea of what the final product should look like. You can't start building a house without a blueprint, and you can't start building an app without an idea or goal. This extends to what you want your app to look like.

One bit of advice I picked up for developing web sites was to take a note card or piece of paper, and draw what you wanted your site to look like. Then, use a different colored pen and draw how a user would interact with the website. I've done this with Forecast and with the app I'll be building in this blog series, and I highly recommend you try it out as well.

Now that you have your app idea, I'll share mine. I am going to be writing a metronome application called Astronome. It will be a simple, one screen interface that will help keep tempo for whatever you are needing a tempo for.

![Paper concept for Astronome](/res/astronome-paper-concept.jpg)

And yes, I am aware I forgot to put a play/pause button on my paper mock up.

## Square Two: Getting Some Code

Coding can be a tedious task, and starting an app from the ground up can be a bit rough at times. Thankfully, there are things called app templates that make our lives a little easier in that area. Luckily for us looking to code in libcosmic, community members and System 76 engineers have come together and created [the Cosmic app template](https://github.com/pop-os/cosmic-app-template) for us to copy and work off of. And we're going to do just that!

Copying it doesn't require a Github account, but having one makes the process a bit easier. If you don't have an account, and don't want one, just download the repository as a ZIP file and extract it to wherever you want the files to reside. Now rename the folder to the name of your new app, preferably all lowercase. Now create a repo on your favorite source control hosting service and sync it up so all the changes can be tracked.

If you do have a Github account, you can select the green 'Use this template' button in the upper right hand corner of the website. In the dropdown that opens, select create new repository. This will take you to a new repository screen where you can name the repository whatever you want. I would name it the name of your app, in all lowercase letters, like below. Give the repo a short description as well.

![Picture of the Use this template button](/res/use-this-template-screenshot.png)

![Picture of the new repo screen](/res/new-repo-screenshot.png)

And now all that's left is to clone the repo to your local machine so we can start coding!

## Square Three: Adjusting The Template

Now that we have code we can work with, we need to change it up to match our app's name and id. There's no quick way to do this, just open each file in the app directories and change the names, ids, links, etc. as you find them. This includes renaming the English translation file, as not doing so will cause the app to throw a few errors on compile time.

Make sure at this point to sync up your changes with GitHub or your preffered git provider. I like to save/sync often so it's a good idea to commit changes as they occur to make sure no work is lost.

I don't want to have this blog post go on too long so I am going to stop here for this tutorial. In the next we will adjust the template to get the UI we want. In the meantime, what do you think of developing in libcosmic so far? Let me know by sending me an email or at Mastodon in the links at the bottom of my website. As always, let me know.

Thanks, Jacob
