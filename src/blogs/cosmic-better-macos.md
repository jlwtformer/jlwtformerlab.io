# How Cosmic DE Is More Exciting Than The Latest macOS Updates

_Monday, July 29, 2024_

A few months ago Apple took to the stage at their World Wide Developer Conference (WWDC for short) to announce a plethora of software updates across all of their hardware offerings. While this is usually an exciting time for most Apple product users, I find myself more interested in a piece of software from a smaller company based out of Denver, Colorado.

That company is System 76. They are a Linux first OEM that sells desktops, laptops, servers, keyboards and a few other goodies. They have taken the time to not only make their own distribution of a desktop Linux OS, but have also taken to making their own Desktop Environment as well.

If you're not familiar with what a Desktop Environment is, it's the user interface that you use to interact with all of your programs. In Windows, this includes your taskbar, icons, start menu, etc. In macOS, this includes your menu bar, dock, icons, etc. For years the System 76 Linux distribution, called Pop OS, has used the Gnome desktop environment. But, as the Gnome developers and System 76 developers have seen an increasing divide in vision for their desktops, System 76 decided to create their own DE, called Cosmic.

So, how is this Cosmic more interesting than what's coming to the future release of macOS? Well, I have a few reasons as to why.

## Window Tiling

One of the features announced at WWDC for macOS was the ability to tile your app windows on your screen using hot corners and keyboard shortcuts. This is a long overdue feature that macOS users have often relied on 3rd party apps to accomplish.

Tiling is one of Pop OS's strong points, and Cosmic is no stranger to those strengths. Cosmic has what's called Auto Tiling, where any open window is automatically tiled to use equal parts of the screen real estate. This automatic tiling has been a feature that I feel is almost essential to my computer use, and I find myself severely missing it when I have to use my Windows based work computer, or when I use macOS on my personal laptop.

The auto tiling feature of Cosmic also includes window stacking, so if you want to layer two or more apps on top of one another in a tile, you can. I typically use this to layer my email clients as shown in the screenshot below.

![Screenshot of joke email to Jeremy Soller](/res/cosmic-tiling-demo.png)

## Configurability

One thing that Apple software is known for is being light on the configurability. What does that mean? Usually the layout of the desktop that Apple ships is what you are stuck with. While iOS saw some increased configurability at WWDC, macOS did not. This is where Linux in general shines, and Cosmic shines even more.

As you could see in the screenshot above, I had my desktop laid out with a single taskbar on the left side of my screen. But that's not the only layout available, it's not even the default layout.

Cosmic consists of two main components: The panel and the dock. In the default layout, the panel is spanned across the top of the screen and the dock is on the bottom, similar to a macOS layout. What you can do with them is endless, every component of each the dock and panel can be moved to any side or center and can be on either the dock or panel. This opens up a world of possibilities for desktop layouts and workflows. Something just not possible on macOS.

## Theming

Another facet of customizability that Apple lacks is a good theming engine. While it is possible to change the accent colors on macOS, the light and dark themes are very static. On Cosmic, you are able to pick any color you can imagine for not just the accent colors, but for the app background and foreground colors as well. This leads to an unlimited possibility of themes.

I personally like matching my desktop colors to the colors picked by Material You on my Android phone, as seen below.

![Themeing example one](/res/cosmic-themeing-one.png)

![Themeing example two](/res/cosmic-themeing-two.png)

## No AI Shenanigans

A good portion of the WWDC, and the macOS preview website, is dedicated to a thing called Apple Intelligence. As you can guess, Apple Intelligence is Apple's answer to the latest tech craze, AI. Now I am personally not a big fan of the AI trend, so this section is very much a personal opinion.

There currently is no AI whatsoever to be found in the Cosmic DE. None. Nada. Zilch. And to me that is great. There's no Language Learning Model to get in the way of my information or to confuse my work. Cosmic was designed to be a tool to directly benefit the user, not designed to please investors that are obsessed with the latest tech trends.

## All The Other Stuff

Now by this point I'm sure you're wondering about all the other features of macOS that I had skipped over. Namely continuity, screen mirroring your iPhone, FaceTime previews, etc. And while Cosmic itself doesn't an answer to these features, it doesn't need them. That's because Cosmic is part of a larger open source ecosystem.

The phone integration? That's been a thing on Linux for a while with the help of KDE Connect. FaceTime previews? That's covered by the video conferencing app of your choice. A lot of the nitty gritty that's new in macOS is reliant on you being deeply engrained in the Apple ecosystem. With Cosmic, it's open to whichever mobile ecosystem you want to be in, whether that be Apple's iOS or Google's Android.

So all in all I find myself more interested in what's shaping up to be Cosmic desktop. Apple has some neat things going for them, but they are no Cosmic. What do you think? Let me know on my socials at Mastodon or by Email. I hope you enjoyed my article, and hope you'll enjoy what's to come in Cosmic DE just like I will.

Thanks, Jacob
