# Dell Inspiron 15 Review: A Decade of Utility

_Friday, April 28, 2023_

The year 2012 was an interesting year for me. I graduated high school, worked on an oil rig, and started college. One seemingly minor event that also happened that year was I was gifted my first personal computer, a Dell Inspiron n5110. This laptop was given to me by my parents as a means to help me in school, and it has done that and more. I've been using that laptop all the way up until today, in early 2023. So, is the laptop still useful after 11 years of operation? Let's take a look.

## The Hardware

![The Dell Inspiron 15-n5110](/res/dell-inspiron-15-overview.jpg)

The build of the Inspiron n5110 is very decidedly plastic. At close to 6 lbs, there's no faux metal or coatings to hide the fact that it is anything other than a giant plastic shell holding the internal components. One downside of the plastic build, in taking the laptop apart over and over throughout the years, some of the screw mounts have snapped off, leaving the screen to be held onto the base by a single screw.

Given the 15.6" diagonal screen and form factor, there is plenty of space on the inside for (at the time) top of the line components. The model I own came with an Intel i5-2450M mobile processor, with 4 cores that max out at 3.1 GHz. This processor combined with the included 6GB of DDR3 RAM (later upgraded to 8GB) was more than enough to help me in completing my engineering homework, and also running stress analysis simulations in my internship.

Speaking of that screen... It was more than adequate 11 years ago for the average productive student. But at only 1366 x 768 in resolution it feels very cramped for today's standards. Luckily I have been able to utilize software to extend the screen real estate, however the sharpness and quality has taken a significant hit to do so.

The input devices on this laptop are one of the mixed bag features to me. The keyboard is chiclet style, with a full number pad on the right side. In 11 years I have yet to have any complaints about the keyboard, even typing up this review they keys don't feel too mushy or worn down. The trackpad on the other hand has not aged well. In 2012 the trackpad was adequate when compared to other laptop offerings. However, in 2023 the trend of larger trackpads has made the Inspiron n5110's trackpad feel like a fisher price knockoff. It's small, and doesn't pick more than two touch inputs at a time. In some cases, the trackpad would not work outright due to driver issues on newer operating systems.

Two things that are present on this 11 year old machine that just isn't common anymore are the expansion, and ports. This laptop allows for the upgrade and replacement of the hard drive, battery, RAM, and CPU. As of the time of writing this, neither Dell nor Apple's flagship computers can upgrade any of those components. The upgrade options on this laptop are one of the reasons I've kept using it for all of these years, having replaced the battery 3 times, the RAM once, and the hard drive more than I can remember. The port selection is nothing to ignore either. It contains up to 3 USB ports, an ethernet jack, VGA and HDMI outputs, a full sized SD card slot, an external SATA port, and a full on DVD/CD-ROM compartment. There are enough ports on this laptop to turn it into a full on desktop if you so choose, which is what I did for a long period of it's life.

## The Software

![Software Overview](/res/dell-inspiron-15-software.jpg)

This laptop launched with the one and only Windows 7. At the time the laptop handled the OS well, and ran just about anything I threw at it. Within a few months of me owning the laptop, Microsoft released the first public beta of the controversial Windows 8. Wanting to see what everyone was talking about, I loaded the beta onto the laptop. It ran about as well as a beta could, which is to say not well at all. I could write an entire article about Windows 8, but I'll keep it short and say that my experience with that OS is what led me to discovering a different OS called Linux.

The first Linux distribution that this laptop ran was Ubuntu 12.04. It was an eye opening experience into the inner workings of drivers and hardware compatibility. I ended up switching back to Windows for a few years until 2016 when I then began what's known as dirstro hopping, running a different number of Linux distributions in rapid succession to find the best working one.

The OS that keeps this laptop alive today is none other than Pop!_OS 22.04. PopOS has been able to breathe new life into this aging computer, shifting performance to what is arguably back to what it was almost 11 years ago. I can run some basic games, as long as no dedicated GPU is required. Aside from the trackpad not being able to pick up more than two fingers at a time, this laptop is still able to keep up with modern computers.

## The Utility

![Utility Overview](/res/dell-inspiron-15-utility.jpg)

As I had stated repeatedly, this laptop has lasted me for over 11 years. The sheer amount of work I have been able to put in with this computer is no joke. I have written just about 20+ different papers, created concept designs and final designs for aircraft, ran stress analysis of steel work, and produced blog and video log content. All in all I owe my professional career and where I am today to this laptop.

However, that isn't to say that it will last forever. With the chassis falling apart and the trackpad becoming less usable, it is getting ever closer to retire my old workhorse. Overall the Dell Inspiron n5110 has stood the test of time and held up mostly well. While I wouldn't recommend you run out and buy a used one for your personal use, I am more than glad to have had this laptop over the last decade and some change.

What do you think about using old laptops until they are falling apart? Do you use an "antique" computer? Let me know on my social media links below, I'd love to hear your feedback. Just as well, there is a donation link below if you would like to support me and help fund me getting a new laptop that isn't hanging on by a thread :)

Thanks, Jacob
