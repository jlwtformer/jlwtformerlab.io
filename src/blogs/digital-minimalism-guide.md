# A Guide To Digital Minimalism

_Sunday, May 7, 2023_

With everything in our world becoming more and more digitally reliant, there has been a growing desire to step back from the screens and reconnect with the immediate world around us. This movement has been dubbed digital minimalism. As the name states, it's about minimizing our use of digital devices. It's something I've been playing with for over a year now, and I thought I would share what I've found on my journey into digital minimalism.

## Intentionality

While the main goal behind digital minimalism is to reduce screen time, going cold turkey on my app/digital usage did nothing to actually help. I found myself re-downloading apps and checking my devices as I got bored. What helped me the most wasn't quitting the services, but rather delegating them to specific devices. The idea of digital minimalism to me shifted from quitting digital life, to thinking about what I value and what goals I have long term. From there, I worked backwards and kept only what was important to those values and goals. What I came up with was apps and services that were intentional to my life. This does include social media services, as I do like to stay connected with friends and family.

As I had mentioned, my solution involved delegating the services I used to specific devices. The total number of devices I use currently is 4 total. My work laptop, my person laptop, my personal desktop, and my smartphone.

The work laptop is provided by the company I work for, so it is easy to delegate all work related tasks to just that computer. I used to have my work email on my smartphone, and would check work tasks on my personal laptop from time to time when I left the laptop at the office. Since delegating all work to just the work laptop, I've been able to hold a more steady work-life balance and focus more on my personal time.

Now how do I manage my digital life in my personal time? Well, the simple answer is my phone is only used for phone calls, messaging, music and camera. Everything else I would need to use a service or app for is done on my laptop and desktop. I've found that keeping my phone as parred back as possible has helped in keeping me focused on my surroundings better, and has made me more productive with the time I do get on my laptop and desktop.

## Adopting This Strategy

I can't guarantee that adopting this strategy of digital minimalism will ultimately help you achieve a less digitally reliant life. However, seeing the benefits it has had on my own life I can't help but recommend it to anyone looking to give digital minimalism a try. Simply delegating my services instead of quitting them actually helped me cut back on them in a meaningful way.

Do you like my approach to digital minimalism? Do you have any comments on how I can improve my strategy? Please let me know at my social medias, or by email. If you like my blog and want to help support it, please feel free to help out at the donation link at the bottom of this page.

Thanks, Jacob.
