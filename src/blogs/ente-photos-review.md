# Review of Ente Photos, A Private Google And iCloud Photos Alternative

_Thursday, August 1, 2024_

_*Updated on Monday, August 5, 2024_

A common pain point amongst most non technical people looking to ditch Google or Apple services or increase their privacy is data storage and backup. While most technical people would be quick to point out that there are plenty of NAS or Self Hosted options, something I was looking for was a direct replacement to Google or iCloud Photos that didn't involve anything more than what those services require from a technical standpoint.

What does that mean? It means that I have been looking for a private photo backup solution that requires nothing from me outside of signing up. While I am a self hosting advocate, it's important that there are non technical solutions for those who don't have the know how/equipment to self host.

And I believe I have found that solution in Ente Photos. Ente is an end to end encrypted photo storage solution developed by a [plethora of wonderful people](https://ente.io/about) around the world. They keep [three copies of your data](https://ente.io/blog/replication-v3/), including one copy in a [refurbished fallout shelter](https://www.scaleway.com/en/blog/meet-fr-par-3/). And that's not all they have going for them.

## Open Source

Ente is fully [open source](https://github.com/ente-io). This includes all desktop and mobile apps, as well as the server software powering the service. More on that latter bit in a second.

Being open source is a pledge of transparency from the team behind Ente. By allowing anyone to view the source code behind all of their products, they are not only allowing everyone to see how it works. They are also allowing their code and software to be audited by anyone who is able to find security or privacy flaws.

While I said that I was searching for a service that did not need to be self hosted, one benifit of the server side software being open source is that you can self host Ente if you so choose. This allows you to put as much storage as you want onto your service if you can't afford the pricing structure.

## Pricing

Speaking of the pricing structure, let's talk about that for a second. As mentioned above you can self host for an unlimited storage amount for free, but what if you only want to explore the sign up route? Well, Ente does offer their own servers for a premium.

Luckily there is a free option like most other photo storage solutions. The free tier is 5 GB, and lasts forever. I'm currently on the free plan, and just the photos from my current phone has nearly filled that space. For reference, Google Photos offers 15GB for free, and Apple's iCloud Photos offers 5GB for free.

Below is a brief comparison of the pricing stuctures compared to Google and iCloud Photos. The prices shown are the monthly cost next to the cap.

| Service | Free Tier | 1st Tier | 2nd Tier | 3rd Tier | 4th Tier |
| ------- | --------- | -------- | -------- | -------- | -------- |
| Ente Photos | 5GB | $2.49/50GB | $4.99/200GB | $9.99/1TB | $19.99/2TB |
| Google Photos | 15GB | $1.99/100GB | $9.99/2TB | $19.99/2TB + AI Features | N/A |
| Apple Photos | 5GB | $0.99/50GB | $2.99/200GB | $9.99/2TB | $29.99/6TB |

While Ente tends to be more pricey than other services, I think the premium is worth having the strong encryption services.

## Feature Parity

Now we know how it stands up cost and maintenance wise, but how do the features compare between Ente and Google/iCloud Photos? In short: It does what it can with the information provided through photo metadata, but still lacks some of what makes organization on the competitors special.

The Ente app on mobile has a neat feature I used on Google Photos, where memories would show at the top of the app. Think the "On this day X years ago..." selections.

![A Picture of the ente photos home screen](/res/ente-photos-home.png)

Sharing is a very simple process, much like that of Google Photos. You have the oppertunity to share an album of photos with anyone else who uses Ente privately, or to share the album publicly. Something I think is useful is you can password lock publicly shared albums so that if the link to it falls in the wrong hands, it still has a layer of protection to go through to get through it.

Something that is missing is the ability to filter photos by whose face is in them. This is actually a feature I miss from Google Photos, as I have kids and being able to see just one kid's photos was a very handy feature. The Ente website does say that you are able to sort photos with on device machine learning, however I've only seen my app sort by time. This could be that I used to wipe metadata from my photos, but I'm not sure.

All in all Ente Photos is a great non technical solution to backing up your photos privately. I am going to attempt to self host this service at some point, so stay tuned for that blog post. What do you think of Ente photos? Are you impressed? Let down? Let me know on my Mastodon or by Email. I look forward to hearing your feedback.

Thanks, Jacob