# Fedora 36 Beta - Seriously Tempting

_Monday, April 11, 2022_

It's beta release season in the Linux community! From Ubuntu to Pop OS to Fedora, just about all of the larger snapshot release cycle distributions are putting out betas for their next major updates. Over the last week I have been using the Fedora 36 Beta, and it is seriously tempting me away from my favorite distro, Pop OS.

## Gnome 42

Fedora has for a long time been one of the go to Linux distributions for those who seek a vanilla Gnome Desktop experience. As with tradition, Fedora 36 has come with the latest release of Gnome, version 42. This has been one of the releases that has a redesign of the default theme, as well as further expanding on the dependency of the new libadwaita system.

For those unaware, the libadwaita move was an attempt by the Gnome team to move the system theming into a software library that developers would use to build their GUI applications. From an end user stand point, this just means there is no longer a light and dark adwaita theme, there is just one theme that will switch between light and dark based on a new dark mode API that is available to users as a toggle in the settings app.

However, not all apps currently target this API, so there are a good number of applications that I use that will stay in light theme no matter what the dark mode toggle is set to. We're still in the transition period of the dark mode API adoption, so I don't see this being a long term issue.

I could go on all day about what I like about Gnome. (Seriously, it's my favorite desktop environment) However Fedora doesn't just come with one desktop. If you use the network installer, called the server install on the website, that will let you pick and install whatever software suites you want. From KDE, XFCE, Mate, Cinnamon, LXDE, i3WM, etc down to which editing suites you want. It's great that Fedora allows the user to select from such a wide variety of software to really tune your system to your needs.

## Not Just A Pretty UI

Behind the Gnome user interface is the underbelly of Fedora. For those not aware, Fedora is based off of Red Hat Enterprise Linux. That means a lot of the package management and build systems are the same as the most used enterprise option for Linux systems. Because Red Hat Linux is so prevalent in the enterprise space, it's been refined and hardened for security much more than other distributions like Arch or Debian. And because of that, Fedora has those strengths as well.

Having this back end lets Fedora offer a wide range of options for what software one can install on their system. Just as well, Fedora uses DNF as a package management solution, which works with RPM files to offer even wider options for software. One common complaint I've had with DNF, and one that seems to be a common take, is that DNF can be very slow when searching the software repositories for available software. However, DNF does allow a knowledgeable user to change _how_ it checks the online repositories, letting you speed it up to your preference.

One other thing I have always had an issue with when using Fedora on my hardware is how the disk is partitioned. Now from what I can tell this isn't an issue for anyone else, however back on Fedora 34 on my desktop, and on the Fedora 36 beta on my laptop, Fedora only partitioned an 1/8 of my SSD for the system, leaving the rest of the space unused. I can and was able to reclaim the space, but it is odd to me that the default partitioning scheme was so odd on not one but two of my machines across different versions. Again I haven't been able to verify that this is an issue for anyone else so I am willing to believe it is an issue with how I use the installer.

## Final Thoughts

At the end of the day I really enjoyed Fedora. I found myself actually wanting to keep using it, even in beta form. When debating if I wanted to go back to Pop on my laptop, I had to actually nit pick for reasons to leave Fedora behind. With that said, I think Fedora and stock Gnome are going to be my go to choice for my laptop going forward, with Pop being my go to for my desktop.

So what do you think about Fedora Linux? Do you agree or disagree with my thoughts? Reach out and let me know, I always welcome any feedback you may have.

Jacob
