# Say Hello To helloSystem

_Monday, December 6, 2021_

![Feature Image](/res/hello_system_review_banner.png)

helloSystem is a relatively new operating system on the block. It was founded by one of the founders of the Appimage Linux packaging system, Simon Peter. It's a fully open alternative to macOS and is aimed to be a welcoming experience to "mere mortals" (or as we call them, casual users). It's based off of FreeBSD, which is based off of BSD, just like the macOS Darwin kernel.

## First Impressions

When first booting into the system, it immediately reminded me of the classic Macintosh UI. It has a full menu bar up top, with a system menu in the left corner and the time, date, and indicators in the right corner. It also has an application dock in the bottom center of the scree, but more on that later. The UI is based around a minimal file management system called Filer. Compared to some of the screenshots in the documentation of earlier builds of helloSystem, it's been scaled back to hearken back to the early days of the Macintosh user interface. In fact it says in the project documentation that it's meant to feel welcoming to macOS users.

Installation was relatively straight forward, pretty standard for most modern operating systems. The installation app wasn't front and center though, it was hidden in the filer system behind a few application folders. I've had it installed on one of my two laptops for about a week's time and I'm happy to say I really do like using it.

However, there are a few caveats to using helloSystem. The first and most prominent is that there isn't really a login screen. When you first boot the computer it will boot strait to a desktop. This might be okay and how they did it back in the classic Macintosh days, but we live in a day and age where computer security is something that needs to be taken seriously. Having that password barrier is something that is necessary, especially with laptops.

There is a keyboard shortcut available for locking the screen, however it just doesn't work. Even when I try to invoke the command from a command line, xdg-screensaver doesn't recognize the lock command, even though it's a command in the xdg-screensaver man page.

Overall the system is simple and does what it sets out to do well, following the "Less, but better" goal in their user experience. I haven't daily driven BSD before, but it brings me back to almost a decade ago in my Linux experience. A lot of the software I use day to day is available right out of the gate, however I still had to build a few things from source code. When it comes to multimedia codecs, I couldn't replay captured screen shares because of missing codecs. I couldn't get the screen shares to play on Linux either, so I am assuming the issue is with the screen recording software that comes with helloSystem out of the box.

I was still able to write up this entire blog post and post it to my blog with all baked in tools. No virtualization, no comparability layers, all native tools. A lot of the standard tools I use, Firefox, Thunderbird, KeePassXC, Libreoffice, GnuPG, etc, were all able to be installed right out of the gate.

Something that I feel like might trip up casual computer users is the absence of an app store. And while this might not seem like a big deal to people who daily drive BSD or Linux, someone coming from macOS may be thrown for a loop, as they are used to installing a majority of software from the App Store. And most software that is available, you won't be able to find it on the application website as a download target most of the time. That's another reason I say it takes me back a decade in Linux use, the usability and potential is there, it just doesn't have the broad appeal to convince app developers to target FreeBSD for desktop usage.

Another thing I like about the helloSystem environment is that it isn't just another desktop environment that you can install on any FreeBSD distribution. It built from the kernel up to be a fully integrated stack for your computer usage.

But the software is still in it's beta life cycle. There were some bugs that I ran into, namely in the dock. I couldn't resize or move the dock or its items. And when I unpin something, pinning it back doesn't mean I get to use the icon as a launcher again. I'd have to navigate the application folders to find the app launcher I was looking for. There is still a lot of work to be done before I feel like I could show it off to my non technical friends to get their impressions on it.

A lot of the negatives and bugs I've talked about are already being worked on. You can keep track of it in the bug reporting part of the system's source code, which is available online in its entirety.

## Wrapping Up

At the end of the day, would I still use this as a daily driver? Well I'd really like to, because I really like the classic macOS look and style. And if it weren't for the gaps in the software, I really would. I do plan to keep using it on the laptop it's currently installed on, which is my test laptop anyway. On my main machines that run Linux, I'm moving away from the Gnome desktop, and am moving to a customized XFCE desktop to match the helloSystem and classic macOS style.

I really feel like helloSystem has potential, and I really want to see it go places. I really like it, I'm going to keep using it, and I feel like you should use it too if you want to see it succeed.

Thanks,
Jacob
