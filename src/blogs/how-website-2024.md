# How I Run My Website (2024 Edition)

_Monday, August 12, 2024_

This blog is over three years old. That's not a short amount of time! It has evolved since it first started, and has also changed in terms of how I maintain it. So I figured it was time for an update, how I maintain my blog and website in general.

Now the origins of this method have not changed from the previous setup. It started as a live copy of my resume, and morphed into something of a social hub/blog. The one thing that has changed is the tools I use to manage it all.

Originally I used a tool called SSG6 to generate the website files from markdown documents. Well, wanting to strengthen my resume in the Rust Coding world, I wrote my own version of SSG6 purely in rust, called [blog-static](https://gitlab.com/jlwtformer/blog-static). It is meant to function the exact same as SSG6, you provide a header file, a footer file, a folder full of markdown documents, and a title and then the program will spit out static HTML files for you.

I wrote it as a learning exercise, becoming more familiar with not only the rust development workflow, but with the git workflow as well. I had posted about it on Mastodon and luckily a wonderful developer by the name of Wesley Moore ([@wezm](https://gitlab.com/wezm) on GitLab) helped me reformat my code and clean up some functions.

The hosting aspect hasn't changed much at all. It's still hosted on GitLab pages, however I don't host manually generated files. I use GitLab's [pipeline actions](https://gitlab.com/jlwtformer/jlwtformer.gitlab.io/-/blob/master/.gitlab-ci.yml) along with a binary of my blog-static program to automatically generate the website any time I upload/update a markdown file.

It's a pretty simplistic method to hosting and running my blog, but it's one that I have stuck to for a while now. I don't foresee any changes to this setup any time soon, but I'll be sure to make a new update post if and when I do!

What do you think about how I run my blog? Smart? Dumb? Let me know on my socials at either Mastodon or Email.

Thanks, Jacob