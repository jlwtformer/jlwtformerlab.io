# Moving From Linux to Mac

_Saturday, June 24, 2023_

My main laptop is 11 years old. As great as it's been throughout the years, it's time to replace it for good. In the 11 years I've had it, it has followed me through college, my early family stages, my early career, and even the start of this blog.

So without further audue, let's introduce my new laptop: The Mid-2015 Macbook Pro. Now if you have followed me for any period of time you would know that I wasn't always a big proponent of Macs, and stuck to Linux for many years for philisophical reasons. Let's talk about why I chose a Mac, and why I went for the older model that I did.

![A Picture Of My Macbook](/res/macbook-pro.jpeg)

## The Ownership Principle

Like living in a rented house vs. owning a home, Mac vs Linux has usually been an argument about software ownership. Using a Mac does indeed "just work", but when things don't work (which isn't often) you can't fix them yourself. While using Linux you are free to fix and change anything you want, but there is a certain amount of technical debt that comes with that. I've spent plenty of hours fixing drivers and rewriting configs to ensure Linux works on my hardware. But at the end of the day my hobby time is limited, so I want something that just works with little fuss.

When I was on Linux I spent a huge amount of time customizing my setup to the point that I wasn't as productive as I could have been with my computer. Having that choice and flexibility hindered me where I needed it. If I had a computer that just had one theme, one setup, etc. then I would have more room for productive tasks that require that extra time.

## The Philosophy

One of the reasons I stuck with Linux for so long was the philosophy of ownership, like described above. However, my views on ownership have changed as I have gotten older, and started getting some silver hairs.

A general tenant of the Buddhist philosophy is to let go of attachments in order to rid one's self of harmful desire. That philosophy is one that I want to subscribe to, so I am attempting to apply it to my view of computers. I valued my life under Linux more than my productivity, and it bit me in the butt a few times. So by getting rid of my attachments to the philosophies of Linux I am freeing my time up for more productive tasks, and freeing up myself from worrying about whether or not a software update will destroy my drivers.

While I do use my computer for very important tasks, my life does not revolve around it. The importance of what I run just does not matter like it used to. That's why I am switching to Mac.

## Why The Hardware

Now I'm sure you are wondering why I chose to purchase an older Macbook instead of one of the newer models. There's a few reasons why.

The first is price. Macbooks are expensive, so shelling out the money for a pro model just isn't a feasible idea at this time. Expecially given that I am not sure I will be staying on a Mac for the forseeable future. Do I like my Mac? Yes I do, but that honeymoon phase could wear off and I may decide to move back to Linux.

That's why I went with an older Intel model Mac. It's one that can run Linux if I so choose to give up on the macOS life. Another reason for the 2015 era model, it doesn't have the dreaded butterfly keyboard. Enough said.

"But isn't the 2015 model no longer supported?" Well this is true. Apple no longer pushes major OS updates to the model of computer I own. But, thanks to the help of projects like OpenCore Legacy Patcher I am able to install and run the latest macOS with little to no hassle. That, and Apple is still pushing security updates to the newest software that this machine supports, so I have a few years of life to save up for an M model Macbook if I so choose.

---

So what do you think about Macs? Do you think I made the right choice? Let me know by reaching out to any of my social accounts, or directly through email.

Thanks, Jacob
