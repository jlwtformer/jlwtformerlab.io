# Creating an RSS Feed for a Static Website

_Friday, July 2, 2021_

One of the ways a content creator can share their blog posts or YouTube videos is with an RSS feed. RSS stands for Really Simple Syndication, which is an apt name for what the service provides. Simply put, a content creator will provide an RSS feed document, and a reader will link that feed document to their preferred RSS reader, and the reader will scan for changes to the document and will notify the reader when there is new content from the creator.

Typically RSS feeds are live documents hosted on a website, that will update automatically when new content is added. As I explained in my post on making a simple website, some web hosting solutions (like Git Pages) will not allow for dynamic content. So, to have an RSS feed, the creator of the static site would need to update the RSS document manually (or automate it behind the scenes). Creating one is actually pretty simple, and I can show you how I made one for this blog with my own.

## Creating an RSS Feed Document

Creating one is about as easy as making a static webpage. The RSS document is just an XML file, often called `feed.xml` that is hosted on your platform. To start, the first line of the XML file will need to have some information about the document itself, and will look something like below:

```XML
<?xml version="1.0" encoding="utf-8"?>
```

The two properties being declared here are self explanatory. A `version` tag to let the reader know what version of XML document it is, and an `encoding` declaration to let the reader know what character encoding the document is written in.

Next we will open up an `rss` tag, which will contain RSS specific properties, and will wrap all of the content of the rest of the RSS Feed document.

```XML
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
</rss>
```

This tag has a few similar properties:

* `version` Declares which RSS version the document is.
* `xmlns:atom` Declares a reference namespace. This is that we can use custom XML tags from the document it links to.

Next we will put a `channel` tag inside the `rss` tag. Inside the `channel` tag we will put more tags that will give information pertaining to our website, as seen below:

```XML
<channel>                  
    <title>Jacob Westall's Blog</title>
    <description>A blog full of junk.</description>
    <link>https://jwestall.com/blog</link>
    <atom:link href="https://jwestall.com/feed.xml" rel="self" type="application/rss+xml"/>
</channel>
```

The tags within `channel` are self explanatory, with exception of the last. The `atom:link` tag is one that holds RSS specific properties, and is using the `link` tag that is found at the link in our namespace declaration in the `rss` tag.

From here on we will place similar tag groups within the `channel` tag that will contain information about each of our blog/content posts.

```XML
<item>             
    <title>How and Why I Run This Website</title>
    <link>https://jwestall.com/how-why-website.html</link>
    <guid>https://jwestall.com/how-why-website.html</guid>
    <pubDate>Wed, 30 Jun 2021 00:00:00 CST</pubDate> 
</item>
```

Some things to note are the `guid` tag and the `pubDate` tag. A GUID is a unique identifier that will differentiate the `item` group from other similar `item` groups. It can be just about anything, but I use the blog post link to keep things simple. The `pubDate` tag is also important, as not just any date can be thrown in. It requires that you use the [RFC-822 date-time format](https://datatracker.ietf.org/doc/html/rfc822#section-5). It's not that hard of a standard to conform to, but an important one none the less.

So with all of our components put together, we get the following XML document:

```XML
<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>                  
    <title>Jacob Westall's Blog</title>
    <description>A blog full of junk.</description>
    <link>https://jwestall.com/blog</link>
    <atom:link href="https://jwestall.com/feed.xml" rel="self" type="application/rss+xml"/>

    <item>             
        <title>Creating an RSS Feed for a Static Website</title>
        <link>https://jwestall.com/making-rss-feed.html</link>
        <guid>https://jwestall.com/making-rss-feed.html</guid>
        <pubDate>Fri, 2 Jul 2021 00:00:00 CST</pubDate> 
    </item>

    <item>             
        <title>How and Why I Run This Website</title>
        <link>https://jwestall.com/how-why-website.html</link>
        <guid>https://jwestall.com/how-why-website.html</guid>
        <pubDate>Wed, 30 Jun 2021 00:00:00 CST</pubDate> 
    </item>

</channel>
</rss>
```

A good way to make sure your RSS Feed document is properly formatted, is to run it through the [W3C Validation Tool](https://validator.w3.org/feed/). This site will analyze your feed document and will let you know what changes will need to be made to make sure it's a valid document.

## Adding an RSS Feed to a Reader

Different readers will have different methods of adding feeds, however most will be as simple as linking the feed document. Simple as that.

If you want to subscribe to my blog, you can find my RSS Feed Doc at `https://jwestall.com/feed.xml`. That way, you won't have to miss any blog posts from yours truly.
