# The Things I Like About Manjaro, And The Thing I Don't

_Wednesday, March 23, 2022_

If you're like me and have been immersed in the Linux community for any amount of time, you've probably heard about a distribution called Manjaro. If you haven't, well then let me introduce you! Manjaro Linux has been popular for quite a few years, and I myself have used it off and on for a few years. Where most of the "beginner friendly" distros are based on Ubuntu/Debian, Manjaro actually bases off of Arch Linux, which is a rolling release distribution.

Where Arch Linux aims to be a more "DIY" distribution, where you are free to chose what pieces of the system to install, Manjaro aims to provide the flexibility of Arch with a stable starting point. It is more than just an Arch Linux installer, providing it's own software repositories and development life cycles.

## What I Like

One thing is made abundantly clear up front, the user is in control of the experience in Manjaro. When you go to download Manjaro they offer three officially supported OS variants, and a plethora of community maintained alternatives. The three official options let you decide from a desktop utilizing either the XFCE, KDE, or Gnome desktop environments.

And no matter what desktop interface you chose from, Manjaro provides you with programs to keep you in control of your experience. In the KDE desktop, the landing page of the settings app has quick toggles to the most common options, even offering a shortcut to change your desktop background. (One of my biggest gripes with KDE is how hidden that settings page is). In the Gnome desktop they have a dedicated 'Layouts' app that will let you quick toggle between 6 different UI configurations, with some other added options as well. Present on all desktops is an app that will let you specifically chose which kernel version you want to run. This has actually been useful for me in the past, where a kernel update borked a part of my system, and I was able to roll back to the previous kernel without having to touch a command line at all.

Manjaro has a lot of these tools baked in all over. In their version of an app center, called Pamac, you can chose to enable the Arch User Repository, enable Flatpak or Snap support. If you prefer to manage your software over the command line, they have the pacman-mirrors utility to update your mirror list automatically.

One of Manjaro's strong suits is it's hardware support. Being based on Arch Linux they often have access to software almost immediately after it's been finished by the original developers, whereas other distributions like Debian or Fedora will wait to release major components at fixed intervals. And speaking of hardware, if you want to buy a computer with Manjaro installed on it, you can! The official Manjaro website has links to do so, which most Linux distributions don't. While this is also something that could be said about Pop!_OS or PureOS, Manjaro's team isn't employed by the makers of the hardware that ships the distro. Manjaro is a fully independent effort that seems to have been picked up by a wide range of different computer manufacturers.

Now if you don't want to shell out the money for new hardware, Manjaro comes with a very easy to use installer. Like just about every other Linux distribution Manjaro's install media can be written to any USB stick of sufficient space. It's a live medium, meaning when you boot into it you will be placed onto a desktop with a nice welcome screen. On that welcome screen you'll find some pretty handy links to documentation and support channels.

The actual install program is straight forward and simple. The disk partitioning screen is one of my favorites, giving a very simple method to install Manjaro alongside whatever operating system is currently installed on your computer.

## What I Don't

Now while Manjaro has a large amount of positives, there is one downside that I have run into when using it. Being based on Arch, Manjaro uses the pacman package manager, which is not one of my favorites to use. Arch and pacman have a tendency to consolidate smaller packages and libraries into one package. This can save some space for those who are looking to pinch out as much disk space as possible, however it creates an awkward dependency issue when two programs depend on one package for different uses. For example, for a long time in Manjaro or any other Arch based distro, uninstalling the "Cheese" camera app from the Gnome spins by default would also uninstall the graphical settings application. While seems like no biggie to those who know their way around the command line, for non technical users they would have no idea what to do.

## Conclusion

At the end of the day, Manjaro is a great distribution. While it does have some hiccups from being based on a rolling release distribution, when Manjaro shines it really shines. But, it's not enough to be my favorite. While there are more positives than negatives, Pop!_OS remains my go to Linux distribution. Really just because I don't like pacman.

What did you think of this blog post? Reach out and let me know, I'm always open to feedback.

Jacob
