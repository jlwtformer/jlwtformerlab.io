# Starting Video Logs

_Friday, October 29, 2021_

Impostor Syndrome, [as defined by the Merriam-Webster dictionary](https://www.merriam-webster.com/words-at-play/what-is-impostor-syndrome), is the belief that one's accomplishments are a result of fraudulence or sheer luck instead of skill. It's the feeling that you are not skilled enough to be in the field or in the hobby you are in, even though you do belong.

I have been dealing with impostor syndrome for quite a while now. Back in college, I helped design an airplane that was designed to deliver a payload from a high altitude to a precise target. As cool as it is, I didn't contribute as much as I would have hoped to. My role on the team was to design the landing gear system. The other team members; Ben, Drew, Teeter, Cameron, Jared, Trent, Connor and a whole other list of faces I can remember, but names I can't; all pitched in to design and build the best aircraft I have personally seen. All I did, was make some bent pieces of metal that held a wheel.

This syndrome has followed me all over the place. At work, at home, even in the blog I started as just a side hobby. Having a bit of a crisis of self, I did what all rational people do, and went to Twitter. I asked if anyone still wants to read my blog, or if I should give a shot at making video logging content. Well, in true fashion for my notoriety, I got two votes to start making videos.

While I am hesitant to want to start making online videos (as this would not be my first attempt at making online video content), I think this will be a good way to break out to wider audience who may not have noticed my blog otherwise.

So what will I be doing videos on? Well, the same thing I cover on this blog. Anything I find interesting. Where will I posting it? The obvious answer is on [YouTube](https://www.youtube.com/channel/UCEOuNtcigFUx8Td6es5jioA), but I don't want to just post my content to one place. I'll be double hosting, on both YouTube and [TilVids](https://tilvids.com/c/jacob_westall_channel/videos). TilVids is part of a decentralized video sharing platform that you can watch me on if you want to avoid Google and the spying that comes with the use of their products.

I may still deal with impostor syndrome from time to time, but I don't want that to effect my hobbies. I don't want to quit writing, creating, coding, etc. Thank you for supporting me and my hobbies thus far, and thank you for your continued support as I continue exploring new avenues of life.

Thanks,
Jacob
