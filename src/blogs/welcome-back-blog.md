# Welcome Back Blog

_Wednesday, July 24, 2024_

Wow, it's been over a year since I've updated my blog. Better late than never, right? 

Anywho, a good bit has happened in the last year. I added on another member to my family, I tried to make a community weather app, I started to write music for drumlines again, and so much more. But one thing I didn't do ... Is update this blog. 

And that was partly on purpose. I had a lot going on in my personal life that took a large amount of time and effort. Unfortunately, my two daughters were found to have a rare genetic disorder called Pompe Disease. If you don't know what that is (I didn't until my daughters were diagnosed) then I would recommend you look at [Duke University's page about it](https://www.dukehealth.org/treatments/pompe-disease). It's a serious thing and has involved seeing multiple doctors a week for months, even up to traveling state lines to see specialty doctors multiple times. It's been a tiring few months, but all in all it's been worth it to spend more time with my family.

But now I'm back to working on my blog and my website. And I'm sure you are curious, what will I cover? Well that's a good question. I plan to continue writing about whatever comes to my mind. Coding, dev tools, privacy, and maybe even some of the process I use to write drum music.

It's been a good break, but it's time to get back to it. Thank you for reading my underwhelming blog, and continuing to share this journey known as life with me.

Thanks, Jacob.
