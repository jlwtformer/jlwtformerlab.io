# Jacob Westall

![profile-photo](/res/me.png)

Research & Development Engineer | Privacy Advocate | Maker of Things

Hobbyist with a passion for making things. What kind of things? All kinds of things. Music, Blogs, Vlogs, Programs, the elusive etc, and so on.

## Contact Me

- [jacob@jwestall.com](mailto:jacob@jwestall.com)

## Some of My Stuff

- [Github](https://github.com/jwestall)
- [GitLab](https://gitlab.com/jlwtformer)
- [Blog](/blog) ([RSS Feed](/feed.xml))
- [YouTube](https://www.youtube.com/channel/UCEOuNtcigFUx8Td6es5jioA)
- [TilVids](https://tilvids.com/c/jacob_westall_channel/videos)

## Social Media

- [Mastodon](https://mathstodon.xyz/@jwestall_com)
- [Instagram](https://instagram.com/jwestall_com)

## PGP Information

- Fingerprint: `60FC 6176 612A 1476 3045 BAA9 6894 8833 CC1F 3677`
- Public Key Import: `curl https://jwestall.com/res/jacobwestall.asc | gpg --import`
